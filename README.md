WordPress Minimal Harderning
============================
Pratiche per creare le condizioni di minima di sicurezza richieste ad un portale Wordpress
------------------------------------------------------------------------------------------

#### Proteggersi dalle modifiche
Una delle prime cose da fare dopo che la struttura del prodotto è stabilita è proteggere eventuali modifiche esterne o non autorizzate. 
Occorre quindi aggiungere a index.php il seguente codice

```
define('DISALLOW_FILE_EDIT',true);   // Disbalita l'editor dei file di wp
define('DISALLOW_FILE_MODS',true);   // Disbalita la modifica dei file dei plugin

```

#### Proteggersi da DOS che sfruttano il pingback
Molti attachi DOS sfruttano delle request verso xmlrpc.php. Per evitare che il carico della macchina aumenti ed evitare che venga usata per attacchi DOS a terzi occorre aggiungere le seguneti istruzioni al file index.php o nel function.php

```
// Disabilita X-Pingback HTTP Header.
add_filter('wp_headers', function($headers, $wp_query){
    if(isset($headers['X-Pingback'])) unset($headers['X-Pingback']); // Drop X-Pingback
    return $headers;
}, 11, 2);
// Disabilita XMLRPC
add_filter('pre_option_enable_xmlrpc', function($state){ return '0'; });
// Rimuove i  rsd_link
add_action('wp', function(){ remove_action('wp_head', 'rsd_link'); }, 9);
// Hijack pingback_url per la funzione get_bloginfo.
add_filter('bloginfo_url', function($output, $property){ return ($property == 'pingback_url') ? null : $output;}, 11, 2);
// Disabilita pingback.ping
add_action('xmlrpc_call', function($method){
    if($method != 'pingback.ping') return;
    wp_die(
        'Pingback functionality is disabled on this Blog.',
        'Pingback Disabled!',
        array('response' => 403)
    );
});
```

#### Security by obscurity
La sicurezza non può essere mai garantita dall'offuscamento o la pratica di nascondere le risorse. Detto questo può essere utile, per evitare di essere immediatamente riconosciuti come un sito attaccabile modificare alcuni comportamenti di default.
##### Nascondere il readme
Provvedere a rimuovere il file readme.html, per evitare di far sapere la versione corrente di wp.
##### Nascondere la versione di Wordpress
Per rimuovere la versione di Wordpress dalle pagine occorre aggiungere un filtro in index.php
```
function wp_remove_version() {
    return '';
}
add_filter('the_generator','wp_remove_version');
```
##### Cambiare l'utente admin
Per evitare di permettere di far riferimento all'utente admin in un possibile attacco conviene abilitare un'altra utenza a tale utilizzo e disabilitare quella originale.

#### 
