<?php // wp-hardening.php


function hardening() {
    // Disable any update to plugin and disable the editor if not "editable" file exists
    if(!file_exists(get_home_path(). '/editable' )) {
        define('DISALLOW_FILE_EDIT',true);   // Disbalita l'editor dei file di wp
        define('DISALLOW_FILE_MODS',true);   // Disbalita la modifica dei file dei plugin
    }


}
// Disabilita XMLRPC
add_filter('pre_option_enable_xmlrpc', function($state){ return '0'; });

// Rimuove i  rsd_link
add_action('wp', function(){ remove_action('wp_head', 'rsd_link'); }, 9);

// Hijack pingback_url per la funzione get_bloginfo.
add_filter('bloginfo_url', function($output, $property){ return ($property == 'pingback_url') ? null : $output;}, 11, 2);

// Disabilita pingback.ping
add_action('xmlrpc_call', function($method){
    if($method != 'pingback.ping') return;
    wp_die(
        'Pingback functionality is disabled on this Blog.',
        'Pingback Disabled!',
        array('response' => 403)
    );
});

// Disabilita X-Pingback HTTP Header.
add_filter('wp_headers', function($headers, $wp_query){
    if(isset($headers['X-Pingback'])) unset($headers['X-Pingback']); // Drop X-Pingback
    return $headers;
}, 11, 2);

// Rimuove informazioni sulla versione
function wp_remove_version() { return '';}
add_filter('the_generator','wp_remove_version');

add_action('wp_loaded','hardening');